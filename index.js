const express = require('express')
const app = express()
const port = 3000
const routes = require("./routes/index")
const Middleware = require("./middleware/index")

app.use(express.static('views/assets'))
app.set("view engine", "ejs")

app.use(express.json())
app.use(express.urlencoded({ extended: true }))

app.use(routes)
app.use(Middleware.errorHandler)

app.all("*", (req, res) => {
  res.send("404 Page Not Found")
})

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})


// app.get('/', (req, res) => {
//   console.log(req.body)
//   console.log(req.headers, "<<")
//   const data = {
//     name: "Kevin"
//   }

//   res.status(200).json(data)
// })
// app.post("/test", (req, res) => {
//   console.log(req.query)
//   res.send("Ini Method POST tanpa ID ")

// })

// //Middleware
// app.use((req, res, next) => {
//   console.log("=== middelware ===")
//   console.log(req.query)
//   console.log(req.params)
//   console.log(req.body)
//   next()
// })

// app.post('/:nama_params/:id', (req, res) => {
//   console.log("==== POST WITH PARAMS")
//   res.send("Ini Method POST dengan id")
// })



// app.put("/", (req, res) => {
//   console.log("=== method put / ===")
//   res.send("INI METHOD PUT")
// })

