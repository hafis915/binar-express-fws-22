
class Middleware {
  static putMiddleware(req, res, next) {
    console.log("=== Middleware  ===")
    console.log(req.body)
    const name = req.body.name
    next()
    // if (name == '') {
    //   // res.send("Harus Kirim Nama")
    // } else {
    //   next()
    // }
  }

  static errorHandler(err, req, res, next) {
    console.log("=== error handler ===")
    console.log(err)
    if (err.status) {
      const _status = err.status
      res.status(_status).json(err)
    }
    else {
      res.status(400).json({
        messages: "Ooppss"
      })
    }
  }

  static checkLocalStorage(req, res, next) {
    console.log("=== check ==")
    const _local = localStorage.getItem("isLogin")

    console.log(_local, "<<<")
    next()
  }
}

module.exports = Middleware